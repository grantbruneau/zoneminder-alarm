# zoneminder-alarm 
This node application creates an HTTP server which listens for post requests. Upon receiving a post request to /alarm it will use telnet to create an alarm in ZoneMinder. The post data should be JSON formatted and include a key named monitor and value. The value should be the friendly name of your monitor defined in monitors.json. This application also has the ability to send push notifications via Pushover.

## Installation Instructions
### Configure application 
 - Download project zip from gitlab.com and extract contents.
 - Rename .env-sample to .env
#### Update .env
 - Set ZM_HOST to your zoneminder server IP address.
 - ZM_ALARM_SECONDS is used as a minimum threshold before triggering new alarms/notifications. Set this to your desired value.
 - If using pushover change PUSHOVER_ENABLED value to true and add your user/token from https://pushover.net/.
#### Update monitors.json
 - Add your monitors to monitors.json and remove the examples. The key is the monitor ID, the value is the monitor name. The monitor name is sent in the post requests json data.

### Configure ZoneMinder
 - Enable OPT_TRIGGERS in the ZoneMinder system options.

### Install docker
 - https://docs.docker.com/get-docker/

### Building the docker image:
 - Open up your desired terminal
 - Navigate to the extracted zoneminder-alarm project
 - Run the command `docker build . -t zoneminder-alarm`

## Start the container manually or in detached mode
### Manually start the container
 - `docker run -p 8080:8080 --name zoneminder-alarm zoneminder-alarm:latest`

### Start the container in detached mode and enable auto restart
 - `docker run -p 8080:8080 --name zoneminder-alarm -d --restart unless-stopped zoneminder-alarm:latest`

## Generating an alarm
 - `curl --header "Content-Type: application/json" --request POST --data '{"monitor":"Front Door"}' http://localhost:8080/alarm`


# Use Cases
 - I use this application to trigger alarms from my external motion sensors.
   - https://www.hackster.io/piotrlewandowskiuk/iot-motion-sensor-with-email-notifications-diy-872a5a
   - Configuration of my [Things On Edge](https://www.thingsonedge.com/) Front Door motion sensor
     - IO1 Wake Up URL: http://192.168.86.2:8080/alarm 
       - URL of this node application, in my case it's running on the same server as ZoneMinder
     - IO1 Wake Up Data: {"monitor":"Front Door"} 
     ![](./TOE.png)
