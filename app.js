require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const rfs = require('rotating-file-stream')
const fs = require('fs');
const app = express();
const alarm = require('./alarm').alarm;
const push = require('./pushover').push;
const monitors = require('./monitors.json');
const helpers = require('./helpers');

//Parse JSON bodies
app.use(express.json());

// create a rotating write stream
var accessLogStream = rfs.createStream('access.log', {
  interval: '1d', // rotate daily
  path: path.join(__dirname, 'logs')
})

// setup the logger
app.use(morgan('combined', { stream: accessLogStream }));

app.get('/', (req, res) => {
  res.send('App online');
})

//Uncomment this route for testing
//app.get('/test', (req, res) => {
//  alarm('Front Door',res);
//})

app.post('/alarm', (req, res) => {
  console.log(req.body);
  var now = parseInt(new Date().getTime() / 1000);

  //Continue if the request included a monitor in the json object
  if(req.body.monitor){
      //Send error if monitor name is not defined in monitors.json
      if(!helpers.getKeyByValue(monitors, req.body.monitor)){
        console.error('Monitor ' + req.body.monitor + ' not found in monitors.json, please update this file and restart the application');
        res.status(500).send('Monitor ' + req.body.monitor + ' not found in monitors.json, please update this file and restart the application');
        return;
      }


      if(global[req.body.monitor]){
        var then = global[req.body.monitor];
      }
      else{
        var force = true;
      }

      //ZM_ALARM_SECONDS is the amount of time in seconds before we can trigger another alarm
      //now - then returns the milliseconds since the last alarm
      if(now - then > process.env.ZM_ALARM_SECONDS || force){
        if(then){
          console.log(`It's been ${now-then} seconds since the last alarm`);
        }
        //Write the current date and time to a global variable so it can be queried in the next alarm
        global[req.body.monitor] = now;

        //Create a ZM Alarm
        alarm(req.body.monitor,res);
        
        //If Pushover is enabled then send a push notification
        if(process.env.PUSHOVER_ENABLED == 'true'){
          push(req.body.monitor);
	}
      }
      //Skipping alarm/notification since minimum time since last alarm set by ZM_ALARM_SECONDS has not passed
      else{
        console.log('Alarm for ' + req.body.monitor + ' was triggered less than ' + process.env.ZM_ALARM_SECONDS + ' seconds ago');
         res.status(413).send('Alarm for ' + req.body.monitor + ' was triggered less than ' + process.env.ZM_ALARM_SECONDS + ' seconds ago')
      }
  }
})

//Create the HTTP server
app.listen(process.env.HTTP_PORT, () => {
  console.log(`Zoneminder external alarm app listening at http://localhost:${process.env.HTTP_PORT}`)
})
