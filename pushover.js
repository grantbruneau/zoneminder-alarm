const Push = require('pushover-notifications');


const push = (source) => {

var p = new Push( {
    user: process.env.PUSHOVER_USER,
    token: process.env.PUSHOVER_TOKEN,
    // httpOptions: {
    //   proxy: process.env['http_proxy'],
    //},
    // onerror: function(error) {},
    // update_sounds: true // update the list of sounds every day - will
    // prevent app from exiting.
  })

  var msg = {
    // These values correspond to the parameters detailed on https://pushover.net/api
    // 'message' is required. All other values are optional.
    message: 'Motion detected at ' + source,	// required
    title: source + ' Motion',
    //sound: 'magic',
    //device: 'devicename',
    //priority: 1,
    html: 1
  }

  p.send( msg, function( err, result ) {
    if ( err ) {
      console.log(err); 
    }
    else{
      console.log('Push notification created');
    }
    
    //console.log( result )
  })

}

exports.push = push;
