const Telnet = require('telnet-client')
const connection = new Telnet()
const monitors = require('./monitors.json');
const helpers = require('./helpers');

var params = {
  host: process.env.ZM_HOST,
  port: process.env.ZM_TRIGGER_PORT,
  negotiationMandatory: false,
  timeout: 0 
}

const alarm = (source,res) => {
  console.log('Creating alarm for',source);
  
  connection.connect(params)
  connection.send(helpers.getKeyByValue(monitors, source) + "|on+" + process.env.ZM_ALARM_SECONDS + "|10|External Motion|"+source, function(err, response) {
    if(err){
      if(err.message == 'response not received'){
        console.log('Alarm created');
        res.status(201).send(source + ' alarm created.');
      }
      else{
        console.log(err.message);
        res.status(500).send(err.message);
      }
    }
    else{
      console.log(response)
    }
  })
  connection.end();
}

exports.alarm = alarm;
