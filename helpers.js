
module.exports = {
//Function to get monitor key based off it's value
  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }
}
